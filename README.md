# QA tests for Demoshop application
## Autor: Iulian Ciobanu


## Descriere proiect

Proiect pentru testarea automata a unei aplicatii Web: https://fasttrackit-test.netlify.app/#/

"QA tests for Demoshop application" este un proiect de testare automata dezvoltat in limbajul Java. Implementarea testelor s-a realizat utilizand framework-ul Selenide impreuna cu TestNG.

Selenide este un framework pentru testare automată, bazat pe Selenium WebDriver care asigură următoarele avantaje:
-	o interfata de programare (API) simpla si concisa pentru teste
-	o gamă larga si puternica de selectori
-	configurare simpla și rapida

TestNG este framework de testare inspirat din JUnit si NUnit, dar care introduce cateva funcționalitati noi care il fac mai puternic si mai usor de utilizat, cum ar fi:
-	adnotari specifice testelor.
-   configurație de testare flexibila.
-	suport pentru testarea bazata pe date (cu @DataProvider).

Desi este folosit cel mai frecvent pentru pentru comunicarea client-server, am decis sa folosesc un fisier JSON pentru a gestiona mai usor selectorii si valorile pentru assertii.

Site-ul Web testat este alcatuit din mai multe pagini Web: pagina principala, pagina de creare a unui cont de utilizator, pagina de autentificare, cosul de cumparaturi etc.

In directorul src/test/java/base au fost definite cateva clase cu functii generale care au fost folosite la constructia testelor.
In directorul src/test/java/test au fost definite clasele de test corespunzatoare: LoginTest, HomePageTest, AddToWishListTest, AddToCartTest etc:
-	Java version 17
-	Apache Maven version 3.8.6
-	Selenide version 6.7.4
-   JSONparser
-   Google Chrome browser
-   IntelliJ Idea Community Edition
-   Css selectors
-   Allure
-   Git
-   TestNG


package base;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import java.time.Duration;

import static org.testng.Assert.*;

import static com.codeborne.selenide.Selenide.$;


public class Modal extends Base{

    public void isModalDisplayed(String selector) {
        SelenideElement modal = $(selector);
        assertTrue(modal.isDisplayed());
    }

    public SelenideElement getModalElement(String selector){
        SelenideElement element = $(selector).shouldBe(Condition.visible, Duration.ofMillis(2000));
        assertTrue(element.isDisplayed());
        return element;
    }
}

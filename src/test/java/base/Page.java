package base;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import static org.testng.Assert.*;

import java.time.Duration;

import static com.codeborne.selenide.Selenide.*;

public class Page extends Base{

    public void openApp() {
        open("https://fasttrackit-test.netlify.app/#/");
    }


    public String getAppTitle() {
        String title = Selenide.title();
        return  title;
    }

    public SelenideElement getPageElement(String pageElement) {
        return $(pageElement).scrollTo();
    }

    public void clickOnPageElement(String selector){
        SelenideElement element= $(selector).scrollTo();
        element.click();
    }

    public ElementsCollection getElementsOnPage(String selector){
        return $$(selector);
    }

    public SelenideElement findElementOnPage(ElementsCollection collection, String value){
        SelenideElement element= collection.findBy(Condition.text(value));
        return element;
    }

    public SelenideElement findLastElementOnPage(ElementsCollection collection, String value){
        SelenideElement element= collection.last();
        return element;
    }

    public SelenideElement findFirstElementOnPage(ElementsCollection collection, String value){
        SelenideElement element= collection.get(1);
        return element;
    }

    public void addProductToWishList(SelenideElement element, String wishlistButtonSelector){
        element.shouldBe(Condition.visible, Duration.ofMillis(1000));
        SelenideElement workingObj= element.ancestor("div",1).$(wishlistButtonSelector);
        workingObj.click();
    }
    public void addProductToCart(SelenideElement element,String addToCartButtonSelector){
        SelenideElement workingObj= element.ancestor("div",1).$(addToCartButtonSelector);
        workingObj.click();
    }

    public void removeProductFromList(SelenideElement element, String removeItemButtonSelector, Boolean cart){
        if(cart) {
            SelenideElement workingObj2 = element.ancestor("div", 0).$(removeItemButtonSelector).scrollTo();
            workingObj2.click();
        }else{
            SelenideElement workingObj2= element.ancestor("div",1).$(removeItemButtonSelector).scrollTo();
            workingObj2.click();
        }
    }

    public String getCartAmount(SelenideElement element, String text){
        ElementsCollection workingObj= element.$$("tr>td");
        return workingObj.findBy(Condition.text(text)).sibling(0).text();
    }

    public void isPaymentInformationSelected(SelenideElement element,String paymentInformation){
        element.shouldHave(Condition.text(paymentInformation));
        assertTrue(element.ancestor("div",0).find(".form-check-input").isSelected());
    }
}
package test;

import base.Base;
import base.Page;
import com.codeborne.selenide.ElementsCollection;
import static org.testng.Assert.*;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;

import java.util.HashMap;

public class AddToCartTest extends Base {
    Page page= new Page();


    @Test(dataProviderClass = Page.class,dataProvider = "dataProvider")
    public void addToCartTest(HashMap<String,String> hashmap){
        page.openApp();
        page.clickOnPageElement(hashmap.get("resetAppButton"));
        ElementsCollection elements=page.getElementsOnPage(hashmap.get("productCardTitleSelector"));
        page.addProductToCart( page.findElementOnPage(elements,hashmap.get("awesomeMetalChairProduct")),
                hashmap.get("addToCartButton"));
        assertEquals(page.getPageElement(hashmap.get("cartCounterValueSelector")).text(),
                hashmap.get("expectedCartCounterValue"));
    }

    @Test(dataProviderClass = Page.class,dataProvider = "dataProvider")
    public void verifyAddedToCartProductPriceTest(HashMap<String,String> hashmap) {
        page.openApp();
        page.clickOnPageElement(hashmap.get("resetAppButton"));
        ElementsCollection elements = page.getElementsOnPage(hashmap.get("productCardTitleSelector"));
        page.addProductToCart(page.findElementOnPage(elements, hashmap.get("awesomeMetalChairProduct")),
                hashmap.get("addToCartButton"));
        page.clickOnPageElement(hashmap.get("userCartPage"));
        page.getCartAmount(page.getPageElement(hashmap.get("cartTable")),"Items total:");
        assertEquals(page.getCartAmount(page.getPageElement(hashmap.get("cartTable")),"Items total:"),
                hashmap.get("awesomeMetalChairProductItemsTotal"));
        assertEquals(page.getCartAmount(page.getPageElement(hashmap.get("cartTable")),"Tax:"),
                hashmap.get("awesomeMetalChairProductTaxTotal"));
        assertEquals(page.getCartAmount(page.getPageElement(hashmap.get("cartTable")),"Total:"),
                hashmap.get("oneAwesomeMetalChairProductTotalAmount"));
    }

    @Test(dataProviderClass = Page.class,dataProvider = "dataProvider")
    @Ignore
    public void verifyMultipleAddedToCartProductPriceTest(HashMap<String,String> hashmap) {
        page.openApp();
        page.clickOnPageElement(hashmap.get("resetAppButton"));
        ElementsCollection elements = page.getElementsOnPage(hashmap.get("productCardTitleSelector"));
        page.addProductToCart(page.findElementOnPage(elements, hashmap.get("awesomeMetalChairProduct")),
                hashmap.get("addToCartButton"));
        page.addProductToCart(page.findElementOnPage(elements, hashmap.get("awesomeSoftShirt")),
                hashmap.get("addToCartButton"));
        page.addProductToCart(page.findElementOnPage(elements, hashmap.get("gorgeousSoftPizza")),
                hashmap.get("addToCartButton"));
        page.clickOnPageElement(hashmap.get("userCartPage"));
        page.getCartAmount(page.getPageElement(hashmap.get("cartTable")),"Items total:");
        assertEquals(page.getCartAmount(page.getPageElement(hashmap.get("cartTable")),"Items total:"),
                hashmap.get("3itemsAddedToCartItemsTotal"));
        /* assuming that the tax is included in price and is specific only to Awesome Metal Chair, next assertion will pass*/
        assertEquals(page.getCartAmount(page.getPageElement(hashmap.get("cartTable")),"Tax:"),
                hashmap.get("3itemsAddedToCartTaxTotal"));
        assertEquals(page.getCartAmount(page.getPageElement(hashmap.get("cartTable")),"Total:"),
                hashmap.get("3itemsAddedToCartItemsTotalAmount"));
    }
}

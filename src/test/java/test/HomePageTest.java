package test;

import base.Base;
import base.Modal;
import base.Page;

import static com.github.automatedowl.tools.AllureEnvironmentWriter.allureEnvironmentWriter;
import static org.testng.Assert.*;

import com.google.common.collect.ImmutableMap;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.util.HashMap;

public class HomePageTest extends Base {
    Page page = new Page();
    Modal modal=new Modal();
    @BeforeSuite
    void setAllureEnvironment() {
        allureEnvironmentWriter(
                ImmutableMap.<String, String>builder()
                        .put("Browser", "Chrome")
                        .put("Browser.Version", "107.0.5304.62")
                        .put("URL", "https://fasttrackit-test.netlify.app/#/")
                        .build());
    }
    @Test(dataProviderClass = Page.class,dataProvider = "dataProvider")
    public void isAppOpenedTest(HashMap<String,String> hashmap) {
        page.openApp();
        assertEquals(page.getAppTitle(),hashmap.get("appTitle"));
    }

    @Test(dataProviderClass = Page.class,dataProvider = "dataProvider")
    public void guestWelcomeMesageTest(HashMap<String,String> hashmap) {
        page.openApp();
        page.clickOnPageElement(hashmap.get("resetAppButton"));
        String actualWelcomeMessage= page.getPageElement(hashmap.get("welcomeMessageSelector")).text();
        String expectedWelcomeMessage= hashmap.get("expectedWelcomeMessage");
        assertEquals(actualWelcomeMessage,expectedWelcomeMessage);
    }

    @Test(dataProviderClass = Page.class,dataProvider = "dataProvider")
    public void logoTest(HashMap<String,String> hashmap) {
        page.openApp();
        page.clickOnPageElement(hashmap.get("resetAppButton"));
        assertTrue(page.getPageElement(hashmap.get("logoSelector")).isDisplayed());
    }

    @Test(dataProviderClass = Page.class,dataProvider = "dataProvider")
    public void helpModalTest(HashMap<String,String> hashmap) {
        page.openApp();
        page.clickOnPageElement(hashmap.get("resetAppButton"));
        page.clickOnPageElement(hashmap.get("helpButton"));
        modal.isModalDisplayed(hashmap.get("modal"));
    }
}

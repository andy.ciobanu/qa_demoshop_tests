package test;

import base.Base;
import base.Page;
import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import static org.testng.Assert.*;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;

import java.util.HashMap;

public class RemoveItemsFromCartTest extends Base {
    Page page= new Page();

    @Test(dataProviderClass = Page.class,dataProvider = "dataProvider")
    public void removeLastItemFromCartTest(HashMap<String,String> hashmap){
        page.openApp();
        page.clickOnPageElement(hashmap.get("resetAppButton"));
        ElementsCollection elements = page.getElementsOnPage(hashmap.get("productCardTitleSelector"));
        page.addProductToCart(page.findElementOnPage(elements, hashmap.get("awesomeMetalChairProduct")),
                hashmap.get("addToCartButton"));
        page.addProductToCart(page.findElementOnPage(elements, hashmap.get("awesomeSoftShirt")),
                hashmap.get("addToCartButton"));
        page.addProductToCart(page.findElementOnPage(elements, hashmap.get("gorgeousSoftPizza")),
                hashmap.get("addToCartButton"));
        page.clickOnPageElement(hashmap.get("userCartPage"));
        ElementsCollection removeButtons = page.getElementsOnPage(hashmap.get("cartProductSelector")).
                shouldBe(CollectionCondition.sizeGreaterThanOrEqual(2));
        page.removeProductFromList(page.findLastElementOnPage(removeButtons, hashmap.get("gorgeousSoftPizzaId")),
                hashmap.get("removeFromCartButton"), true);
        assertTrue(removeButtons.toString().contains(hashmap.get("awesomeMetalChairProduct"))
                && removeButtons.toString().contains(hashmap.get("awesomeSoftShirt")));
    }

    @Test(dataProviderClass = Page.class,dataProvider = "dataProvider")
    @Ignore
    public void removeFirstItemFromCartTest(HashMap<String,String> hashmap){
        page.openApp();
        page.clickOnPageElement(hashmap.get("resetAppButton"));
        ElementsCollection elements = page.getElementsOnPage(hashmap.get("productCardTitleSelector"));
        page.addProductToCart(page.findElementOnPage(elements, hashmap.get("awesomeMetalChairProduct")),
                hashmap.get("addToCartButton"));
        page.addProductToCart(page.findElementOnPage(elements, hashmap.get("awesomeSoftShirt")),
                hashmap.get("addToCartButton"));
        page.addProductToCart(page.findElementOnPage(elements, hashmap.get("gorgeousSoftPizza")),
                hashmap.get("addToCartButton"));
        page.clickOnPageElement(hashmap.get("userCartPage"));
        ElementsCollection removeButtons = page.getElementsOnPage(hashmap.get("cartProductSelector"));
        Selenide.sleep(1000);
        page.removeProductFromList(page.findFirstElementOnPage(removeButtons, hashmap.get("awesomeMetalChairProductId")),
                hashmap.get("removeFromCartButton"),true);
        assertTrue(removeButtons.toString().contains(hashmap.get("awesomeSoftShirt"))
                && removeButtons.toString().contains(hashmap.get("gorgeousSoftPizza")));
    }

}

package test;

import base.Base;
import base.Page;
import com.codeborne.selenide.ElementsCollection;
import static org.testng.Assert.*;
import org.testng.annotations.Test;

import java.util.HashMap;

public class RemoveItemsFromWishListTest extends Base {
    Page page =new Page();
    @Test(dataProviderClass = Page.class,dataProvider = "dataProvider")
    public void removeFromWishListTest(HashMap<String,String> hashmap){
        page.openApp();
        page.clickOnPageElement(hashmap.get("resetAppButton"));
        ElementsCollection elements=page.getElementsOnPage(hashmap.get("productCardTitleSelector"));
        page.addProductToWishList( page.findElementOnPage(elements,hashmap.get("awesomeMetalChairProduct")),
                hashmap.get("addToWishListButton"));
        page.addProductToWishList( page.findElementOnPage(elements,hashmap.get("awesomeSoftShirt")),
                hashmap.get("addToWishListButton"));
        page.addProductToWishList( page.findElementOnPage(elements,hashmap.get("gorgeousSoftPizza")),
                hashmap.get("addToWishListButton"));
        page.clickOnPageElement(hashmap.get("userWishlistPage"));
        ElementsCollection removeButtons = page.getElementsOnPage(hashmap.get("productCardTitleSelector"));
        page.removeProductFromList(page.findLastElementOnPage(removeButtons, hashmap.get("gorgeousSoftPizzaId")),
                hashmap.get("removeFromWishListButton"), false);
        ElementsCollection wishlistElements=page.getElementsOnPage(hashmap.get("productCardTitleSelector"));
        assertFalse(wishlistElements.toString().contains(hashmap.get("gorgeousSoftPizza")));
        assertTrue(wishlistElements.toString().contains(hashmap.get("awesomeMetalChairProduct")) &&
                wishlistElements.toString().contains(hashmap.get("awesomeSoftShirt")));
    }
}

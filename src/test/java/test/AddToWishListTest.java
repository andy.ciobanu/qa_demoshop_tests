package test;

import base.Base;
import base.Page;
import com.codeborne.selenide.ElementsCollection;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;

public class AddToWishListTest extends Base {
    Page page = new Page();

    @Test(dataProviderClass = Page.class,dataProvider = "dataProvider")
    public void addToWishListTest(HashMap<String,String> hashmap){
        page.openApp();
        page.clickOnPageElement(hashmap.get("resetAppButton"));
        ElementsCollection elements=page.getElementsOnPage(hashmap.get("productCardTitleSelector"));
        page.addProductToWishList( page.findElementOnPage(elements,hashmap.get("awesomeMetalChairProduct")),
                hashmap.get("addToWishListButton"));
        Assert.assertEquals(page.getPageElement(hashmap.get("wishlistCounterValueSelector")).text(),
                hashmap.get("expectedWishlistCounterValue"));
    }

    @Test(dataProviderClass = Page.class,dataProvider = "dataProvider")
    public void addAnotherProductToWishListTest(HashMap<String,String> hashmap){
        page.openApp();
        page.clickOnPageElement(hashmap.get("resetAppButton"));
        ElementsCollection elements=page.getElementsOnPage(hashmap.get("productCardTitleSelector"));
        page.addProductToWishList( page.findElementOnPage(elements,hashmap.get("awesomeMetalChairProduct")),
                hashmap.get("addToWishListButton"));
        page.addProductToWishList( page.findElementOnPage(elements,hashmap.get("awesomeSoftShirt")),
                hashmap.get("addToWishListButton"));
        Assert.assertEquals(page.getPageElement(hashmap.get("wishlistCounterValueSelector")).text(),
                hashmap.get("2productsExpectedWishlistCounterValue"));
    }
}

package test;

import base.Base;
import base.Modal;
import base.Page;
import com.codeborne.selenide.ElementsCollection;
import static org.testng.Assert.*;
import org.testng.annotations.Test;

import java.util.HashMap;

public class CheckoutTest extends Base {
    Page page = new Page();
    Modal modal= new Modal();

    @Test(dataProviderClass = Page.class,dataProvider = "dataProvider")
    public void cashOnDeliveryPaymentInformationTest(HashMap<String,String> hashmap){
        page.openApp();
        page.clickOnPageElement(hashmap.get("resetAppButton"));
        page.clickOnPageElement(hashmap.get("loginButton"));
        modal.getModalElement(hashmap.get("userNameInputField")).sendKeys(hashmap.get("dinoUser"));
        modal.getModalElement(hashmap.get("passwordInputField")).sendKeys(hashmap.get("password"));
        modal.getModalElement(hashmap.get("modalLogionButton")).click();
        ElementsCollection elements = page.getElementsOnPage(hashmap.get("productCardTitleSelector"));
        page.addProductToCart(page.findElementOnPage(elements, hashmap.get("awesomeMetalChairProduct")),
                hashmap.get("addToCartButton"));
        page.clickOnPageElement(hashmap.get("userCartPage"));
        page.getCartAmount(page.getPageElement(hashmap.get("cartTable")),"Items total:");
        assertEquals(page.getCartAmount(page.getPageElement(hashmap.get("cartTable")),"Items total:"),
                hashmap.get("awesomeMetalChairProductItemsTotal"));
        assertEquals(page.getCartAmount(page.getPageElement(hashmap.get("cartTable")),"Tax:"),
                hashmap.get("awesomeMetalChairProductTaxTotal"));
        assertEquals(page.getCartAmount(page.getPageElement(hashmap.get("cartTable")),"Total:"),
                hashmap.get("oneAwesomeMetalChairProductTotalAmount"));
        page.clickOnPageElement(hashmap.get("checkoutButton"));
        ElementsCollection paymentInformation = page.getElementsOnPage(hashmap.get("paymentInformationSection"));
        assertTrue(paymentInformation.toString().contains(hashmap.get("cashOnDeliveryOption")));
        page.isPaymentInformationSelected(paymentInformation.get(1),hashmap.get("cashOnDeliveryOption"));
    }

    @Test(dataProviderClass = Page.class,dataProvider = "dataProvider")
    public void choochooDeliveryInformationTest(HashMap<String,String> hashmap){
        page.openApp();
        page.clickOnPageElement(hashmap.get("resetAppButton"));
        page.clickOnPageElement(hashmap.get("loginButton"));
        modal.getModalElement(hashmap.get("userNameInputField")).sendKeys(hashmap.get("dinoUser"));
        modal.getModalElement(hashmap.get("passwordInputField")).sendKeys(hashmap.get("password"));
        modal.getModalElement(hashmap.get("modalLogionButton")).click();
        ElementsCollection elements = page.getElementsOnPage(hashmap.get("productCardTitleSelector"));
        page.addProductToCart(page.findElementOnPage(elements, hashmap.get("awesomeMetalChairProduct")),
                hashmap.get("addToCartButton"));
        page.clickOnPageElement(hashmap.get("userCartPage"));
        page.getCartAmount(page.getPageElement(hashmap.get("cartTable")),"Items total:");
        assertEquals(page.getCartAmount(page.getPageElement(hashmap.get("cartTable")),"Items total:"),
                hashmap.get("awesomeMetalChairProductItemsTotal"));
        assertEquals(page.getCartAmount(page.getPageElement(hashmap.get("cartTable")),"Tax:"),
                hashmap.get("awesomeMetalChairProductTaxTotal"));
        assertEquals(page.getCartAmount(page.getPageElement(hashmap.get("cartTable")),"Total:"),
                hashmap.get("oneAwesomeMetalChairProductTotalAmount"));
        page.clickOnPageElement(hashmap.get("checkoutButton"));
        ElementsCollection paymentInformation = page.getElementsOnPage(hashmap.get("deliveryInformationSection"));
        System.out.println(paymentInformation);
        assertTrue(paymentInformation.toString().contains(hashmap.get("choochooDeliveryOption")));
        page.isPaymentInformationSelected(paymentInformation.get(0),hashmap.get("choochooDeliveryOption"));
    }

    @Test(dataProviderClass = Page.class,dataProvider = "dataProvider")
    public void tryToCheckoutWithoutCompletingFirstNameFieldTest(HashMap<String,String> hashmap) {
        page.openApp();
        page.clickOnPageElement(hashmap.get("resetAppButton"));
        page.clickOnPageElement(hashmap.get("loginButton"));
        modal.getModalElement(hashmap.get("userNameInputField")).sendKeys(hashmap.get("dinoUser"));
        modal.getModalElement(hashmap.get("passwordInputField")).sendKeys(hashmap.get("password"));
        modal.getModalElement(hashmap.get("modalLogionButton")).click();
        ElementsCollection elements = page.getElementsOnPage(hashmap.get("productCardTitleSelector"));
        page.addProductToCart(page.findElementOnPage(elements, hashmap.get("awesomeMetalChairProduct")),
                hashmap.get("addToCartButton"));
        page.clickOnPageElement(hashmap.get("userCartPage"));
        page.clickOnPageElement(hashmap.get("checkoutButton"));
        page.getPageElement(hashmap.get("lastNameInputField")).sendKeys(hashmap.get("lastNameValue"));
        page.getPageElement(hashmap.get("addressInputField")).sendKeys(hashmap.get("addressValue"));
        page.clickOnPageElement(hashmap.get("continueCheckoutButton"));
        ElementsCollection errors=page.getElementsOnPage(hashmap.get("errorSelector"));
        assertTrue(errors.toString().contains(hashmap.get("firstNameError")));
    }

    @Test(dataProviderClass = Page.class,dataProvider = "dataProvider")
    public void tryToCheckoutWithoutCompletingLastNameFieldTest(HashMap<String,String> hashmap) {
        page.openApp();
        page.clickOnPageElement(hashmap.get("resetAppButton"));
        page.clickOnPageElement(hashmap.get("loginButton"));
        modal.getModalElement(hashmap.get("userNameInputField")).sendKeys(hashmap.get("dinoUser"));
        modal.getModalElement(hashmap.get("passwordInputField")).sendKeys(hashmap.get("password"));
        modal.getModalElement(hashmap.get("modalLogionButton")).click();
        ElementsCollection elements = page.getElementsOnPage(hashmap.get("productCardTitleSelector"));
        page.addProductToCart(page.findElementOnPage(elements, hashmap.get("awesomeMetalChairProduct")),
                hashmap.get("addToCartButton"));
        page.clickOnPageElement(hashmap.get("userCartPage"));
        page.clickOnPageElement(hashmap.get("checkoutButton"));
        page.getPageElement(hashmap.get("firstNameInputField")).sendKeys(hashmap.get("firstNameValue"));
        page.getPageElement(hashmap.get("addressInputField")).sendKeys(hashmap.get("addressValue"));
        page.clickOnPageElement(hashmap.get("continueCheckoutButton"));
        ElementsCollection errors=page.getElementsOnPage(hashmap.get("errorSelector"));
        assertTrue(errors.toString().contains(hashmap.get("lastNameError")));
    }

    @Test(dataProviderClass = Page.class,dataProvider = "dataProvider")
    public void tryToCheckoutWithoutCompletingAddressFieldTest(HashMap<String,String> hashmap) {
        page.openApp();
        page.clickOnPageElement(hashmap.get("resetAppButton"));
        page.clickOnPageElement(hashmap.get("loginButton"));
        modal.getModalElement(hashmap.get("userNameInputField")).sendKeys(hashmap.get("dinoUser"));
        modal.getModalElement(hashmap.get("passwordInputField")).sendKeys(hashmap.get("password"));
        modal.getModalElement(hashmap.get("modalLogionButton")).click();
        ElementsCollection elements = page.getElementsOnPage(hashmap.get("productCardTitleSelector"));
        page.addProductToCart(page.findElementOnPage(elements, hashmap.get("awesomeMetalChairProduct")),
                hashmap.get("addToCartButton"));
        page.clickOnPageElement(hashmap.get("userCartPage"));
        page.clickOnPageElement(hashmap.get("checkoutButton"));
        page.getPageElement(hashmap.get("firstNameInputField")).sendKeys(hashmap.get("firstNameValue"));
        page.getPageElement(hashmap.get("lastNameInputField")).sendKeys(hashmap.get("lastNameValue"));
        page.clickOnPageElement(hashmap.get("continueCheckoutButton"));
        ElementsCollection errors=page.getElementsOnPage(hashmap.get("errorSelector"));
        assertTrue(errors.toString().contains(hashmap.get("addressError")));
    }
}

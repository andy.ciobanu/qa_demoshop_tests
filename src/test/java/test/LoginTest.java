package test;

import base.Base;
import base.Modal;
import base.Page;
import static org.testng.Assert.*;
import org.testng.annotations.Test;

import java.util.HashMap;

public class LoginTest extends Base {
    Page page = new Page();
    Modal modal= new Modal();

    @Test(dataProviderClass = Page.class,dataProvider = "dataProvider")
    public void isModalDisplayedTest(HashMap <String,String> hashmap){
        page.openApp();
        page.clickOnPageElement(hashmap.get("loginButton"));
        modal.isModalDisplayed(hashmap.get("modal"));
    }

    @Test(dataProviderClass = Page.class,dataProvider = "dataProvider")
    public void loginWithValidCredentialsTest(HashMap <String,String> hashmap){
        page.openApp();
        page.clickOnPageElement(hashmap.get("resetAppButton"));
        page.clickOnPageElement(hashmap.get("loginButton"));
        modal.isModalDisplayed(hashmap.get("modal"));
        modal.getModalElement(hashmap.get("userNameInputField")).sendKeys(hashmap.get("dinoUser"));
        modal.getModalElement(hashmap.get("passwordInputField")).sendKeys(hashmap.get("password"));
        modal.getModalElement(hashmap.get("modalLogionButton")).click();
        assertEquals(page.getPageElement(hashmap.get("welcomeMessageSelector")).text(),
                hashmap.get("expectedDinoUserWelcomeMessage"));
    }

    @Test(dataProviderClass = Page.class,dataProvider = "dataProvider")
    public void loginWithInvalidCredentialsTest(HashMap <String,String> hashmap){
        page.openApp();
        page.clickOnPageElement(hashmap.get("resetAppButton"));
        page.clickOnPageElement(hashmap.get("loginButton"));
        modal.isModalDisplayed(hashmap.get("modal"));
        modal.getModalElement(hashmap.get("userNameInputField")).sendKeys(hashmap.get("invalidUsername"));
        modal.getModalElement(hashmap.get("passwordInputField")).sendKeys(hashmap.get("invalidPassword"));
        modal.getModalElement(hashmap.get("modalLogionButton")).click();
        assertEquals(modal.getModalElement(hashmap.get("authenticationErrorSelector")).text(),
                hashmap.get("authenticationErrorMessage"));
    }

}
